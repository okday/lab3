const mysql = require('mysql2');
const retry = require('async-retry');

const db = mysql.createPool({
  connectionLimit: 10,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});

const connectToDatabase = async () => {
  await retry(async (bail) => {
    return new Promise((resolve, reject) => {
      db.getConnection((err, connection) => {
        if (err) {
          console.error('Database connection failed:', err.stack);
          reject(err);
        } else {
          console.log('Database connected as id ' + connection.threadId);
          connection.release();
          resolve();
        }
      });
    });
  }, {
    retries: 10,        // Increase the number of retries
    minTimeout: 5000,   // Increase the time between retries
    onRetry: (error) => {
      console.log('Retrying connection:', error);
    }
  });
};

connectToDatabase();

module.exports = {db};
